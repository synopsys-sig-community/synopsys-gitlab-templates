# Coverity GitLab Runner

This Docker image is provided as an example of how to construct a GitLab runner that includes the necessary bits
for running a full Coverity workflow within GitLab. For the traditional Coverity workflow, including a local cov-analyze
running on the runner itself, it is recommended to use a self-hosted runner due to the large footprint (3+ GB) of the
analysis software.

Building the image:
1. Copy your coverity analysis kit (e.g. cov-analysis-...) and license file (license.dat) to your working directory.
2. Edit the `Dockerfile` and make sure the Coverity version is set to match your analysis kit
3. Build the image: `docker build -t coverity-gitlab-runner .`

The image is set up to automatically bootstrap the latest GitLab Runner software and connect to your account to start
servicing jobs. You will need to pass the following environment variables to the image:

| Environment Variable Name | Description |
| --- | --- |
| **GITLAB_URL** | The URL for your GitLab instance - can be gitlab.com or a self-hosted instance |
| **GITLAB_REG_TOKEN** | The CI registration token generated from the GitLab CI/CD Settings page |

Running the image will vary depending on your platform, but for a simple command line Docker instance it may
look like:

```bash
docker run --name coverity-gitlab-runner \
     -e GITLAB_URL=<URL for your GitLab instance> \
     -e GITLAB_REG_TOKEN=<Your CI Registration Token> \
     coverity-gitlab-runner
```