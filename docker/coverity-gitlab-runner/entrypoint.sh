#!/bin/sh

echo INFO: Using URL: ${GITLAB_URL}
echo INFO: Using Registration Token: ${GITLAB_REG_TOKEN}

echo EXEC: export PATH=/cov-analysis-linux64/bin:$PATH
export PATH=/cov-analysis-linux64/bin:$PATH

echo EXEC: gitlab-runner register -n \
  --url ${GITLAB_URL} \
  --registration-token ${GITLAB_REG_TOKEN} \
  --executor shell \
  --description "Coverity Gitlab Runner"

gitlab-runner register -n \
    --url ${GITLAB_URL} \
    --registration-token ${GITLAB_REG_TOKEN} \
    --executor shell \
    --description "Coverity Gitlab Runner"

cleanup() {
    gitlab-runner verify --delete -t ${GITLAB_REG_TOKEN} -u ${GITLAB_URL}
}

trap 'cleanup; exit 130' INT
trap 'cleanup; exit 143' TERM

echo INFO: Runing gitlab-runner
/home/gitlab-runner/run.sh "$*" &

wait $!
