# Black Duck GitLab Template

The Black Duck GitLab Template provides a fully functioning reference implementation of Black Duck
running within a GitLab CI pipeline, implemented as a fully documented configuration as code template.

Synopsys Detect is a relatively simple utility that handles most of the complexity of running
Black Duck so it does not need to be encoded into this template. You must configure the basic parameters,
and anything else - any Detect command line arguments - may be passed by adding environment variables.

For example, `DETECT_DIAGNOSTIC=true` would enable diagnostic mode.

## Quick Start

To get started, you can include the template in your pipelines by referencing it as a remote template:

```
include:
  - remote: 'https://gitlab.com/synopsys-sig-community/synopsys-gitlab-templates/-/raw/main/templates/blackduck.yml'
```

The template assumes the following CI variables to be set, providing the location and credentials for your Black Duck
Hub instance:

| Variable | Description |
| --- | --- |
| BLACKDUCK_URL | URL to your Black Duck Hub instance |
| BLACKDUCK_API_TOKEN | API Token for authenticating to Black Duck Hub |

The template has a few other options that may be set as variables in your local `gitlab-ci.yml`:

| Option | Default | Description |
| --- | --- | --- |
| DETECT_VERSION | 7.14.0 | Specify the version of detect to use. We recommend settin a default in the template itself, rather than always using the latest in case of unexpected breaking synatx changes |

You can specify these variables in your local `.gitlab-ci.yml`, for example:

```
stages:
- build
- test

variables:
  DETECT_DIANOSTIC: "true"
  DETECT_VERSION: "7.10.0"

# Your local jobs go here
build-job:
  stage: build
  script:
    - mvn package

include:
  - remote: 'https://gitlab.com/synopsys-sig-community/synopsys-gitlab-templates/-/raw/main/templates/blackduck.yml'
```

This template downloads all of the utilities that it needs, and can run in any GitLab runner with curl and Java installed.

### Rapid Scans for Merge Requests

The template will apply Black Duck Rapid Scans for fast turnaround of merge requests.

#### Pushes and Full Scans

To ensure that the Hub database stays up to date and can notify you of KB ubdates that reflect your projects, a full or intelligent
scan is run on pushes to main branches.
