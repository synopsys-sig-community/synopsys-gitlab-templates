# Coverity GitLab Template

The Coverity GitLab Template provides a fully functioning reference implementation of Coverity running within a GitLab
CI pipeline, implemented as a fully documented configuration as code template. 
In this template we have attempted to document all of our known Coverity & GitLab best practices, and provide options
so that you can easily enable/disable various components. However, Coverity supports a very wide range of deployment
scenarios, and it is possible that this will not meet your needs as-is. In such case, you are welcome to clone or fork
this repo and modify to suit your needs - you can then reference your copy from your pipeline!

## Quick Start

To get started, you can include the template in your pipelines by referencing it as a remote template:

```
include:
  - remote: 'https://gitlab.com/synopsys-sig-community/synopsys-gitlab-templates/-/raw/main/templates/coverity.yml'
```

The template assumes the following CI variables to be set, providing the location and credentials for your Coverity
Connect instance:

| Variable | Description |
| --- | --- |
| COVERITY_URL | URL to your Coverity Connect instance |
| COV_USER | User name for authenticating to Coverity Connect |
| COVERITY_PASSPHRASE | Password for authenticating to Coverity Connect |

The template has a number of options that may be set as variables in your local `gitlab-ci.yml`:

| Option | Default | Description |
| --- | --- | --- |
| COVERITY_STREAM_NAME | Not set | Specify the name of the Coverity stream to commit analysis results to, or for the baseline used by incremental analysis.<br><br>If left blank, will default to:<br>`$CI_PROJECT_NAME-$CI_COMMIT_BRANCH` for full analysis<br><br>and: `CI_PROJECT_NAME-$CI_MERGE_REQUEST_TARGET_BRANCH_NAME` for incremental analysis.  |
| COVERITY_PROJECT_NAME | Not set | Specify the name of the Coverity project to consult for looking up analysis results.<br><br>If left blank, will default to:<br>`$CI_PROJECT_NAME`
| COVERITY_OPT_CREATE_STREAM_AND_PROJECT | `false` | (Optional) If set to `true`, the necessary streams and projects will be created on the fly in Coverity Connect |
| COVERITY_OPT_BUILD_COMMAND | Not set | (Optional) Specify the build command. If not specified, source code and dependencies will be inferred automatically |
| COVERITY_OPT_ANALYSIS_OPTIONS | Not set | (Optional) Specify options to be passed directly to cov-analyze |
| COVERITY_OPT_BUILD_OPTIONS | Not set | (Optional) Specify options to be passed directly to cov-build (if using build capture) |
| COVERITY_OPT_SECURITY_GATE_VIEW_NAME | Not set | (Optional) Specify the saved view in Coverity Connect to use for the **push** security gate |

You can specify these variables in your local `.gitlab-ci.yml`, for example:

```
stages:
- build
- test

variables:
  COVERITY_OPT_CREATE_STREAM_AND_PROJECT: "true"
  COVERITY_OPT_SECURITY_GATE_VIEW_NAME: "OWASP Web Top 10"
  COVERITY_OPT_BUILD_COMMAND: mvn package

# Your local jobs go here
build-job:
  stage: build
  script:
    - mvn package

include:
  - remote: 'https://gitlab.com/synopsys-sig-community/synopsys-gitlab-templates/-/raw/main/templates/coverity.yml'
```

**Note:** This template assumes that it will be running in the context of a self-hosted GitLab runner, with the Coverity tools
installed, configured and available in the PATH. Due to the large footprint of the traditional Coverity toolchain, we
recommend using a self-hosted runner. A separate template will be made available for use with the new Coverity Scan Service.

## Capabilities

The Coverity GitLab Template supports the following capabilities:

### Synthesize Stream and Project Names

In order to speed deployment to new projects, the template can create stream and project names automatically based on the
GitLab environment. 

You can override these synthetic names by overriding the appropriate variables in the pipeline.

### Automatically Create Streams and Projects

Also in order to speed deployment to new projects, the template can create the streams and projects if they do not exist
on the Coverity Connect server. This can be very helpful for on-boarding new projects - you can deploy without having any
project specific data in the GitLab workflows, and without doing any pre-preparation on the server. Simply install the template
and go!

The Coverity Connect credentials used must have permission to manage streams and projects.

Automatic stream and project creation is off by default and must be enabled by setting the `COVERITY_OPT_CREATE_STREAM_AND_PROJECT`
variable to `true`.

### Build Capture

Coverity has the ability to analyze your code with or without a build. Running with a build can provide
more accurate results, as we identify precisely the components that go into your shipping software, whereeas
if we run without a build we will leverage the source code and package manager for information about how your
software is composed. This can lead to assumptions and less accurate or complete results.

The default behavior of the template, if no build command is specified, is to automatically find the source
code and dependencies based on source code and package manager files.

To enable build capture, please specify the build command in the `COVERITY_OPT_BUILD_COMMAND` variable.

### Incremental Analysis for Pull Requests

The template will apply Coverity incremental analysis (cov-run-desktop) instead of a full analysis (cov-analyze) when running
in the context of a pull request. This will provide fast turnaround time, and focus developers on the newly introduced issues
rather than everything in the project.

**Note:** A full capture is still used during pull requests - only the analysis step is incremental. For many projects
this is a good trade-off - Java for example tends to have fast build times, and by capturing the entire project the incremental
analysis will have to make fewer assumptions about the code being analyzed, leading to higher confidence results.

### Security Gate

The template has the ability to "fail the build" if a security policy is not met. This is implemented as a simple `exit 1` during
pipeline execution.

#### Pushes and Full Scans

To enable a security gate on pushes with full scans, first configure a saved view in Coverity Connect that represents your security
policy. Any defects that match the saved view's filters, for the project being tested, will be counted as policy violations:

![Coverity Saved View Screenshot](.images/coveritySavedView.png)

Specify the name of the saved view in the `COVERITY_OPT_SECURITY_GATE_VIEW_NAME` variable.

#### Pull Requests and Incremental Analysis

Due to the typically low volume of findings returned from an incremental analysis, the security policy for pull requests
with incremental analysis is any *new* issues.